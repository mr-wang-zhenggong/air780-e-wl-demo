#include "common_api.h"
#include "luat_rtos.h" //luat 头文件 封装FreeRTOS
#include "luat_debug.h"//luat DBUG 库
#include "luat_pm.h" //电源管理库


//本例程，主要测试WAKEUP 

/**********************************WAKEUP 中断测试****************************************************/

/**********************************WAKEUP 中断回调函数************************************************/

void Wakeup_callback_Fun(LUAT_PM_WAKEUP_PAD_E num)
{
    LUAT_DEBUG_PRINT("LUAT_PM_WAKEUP_PAD_E%d",num);
}

/**********************************WAKEUP 中断回调函数************************************************/


void Wakeup0_Fun_Init(void) //Wakeup 0,3,4 初始化
{
    luat_pm_wakeup_pad_set_callback(Wakeup_callback_Fun);
    luat_pm_wakeup_pad_cfg_t wakeup_0_cfg;
    wakeup_0_cfg.neg_edge_enable=1;//使能下降沿
    wakeup_0_cfg.pos_edge_enable=0;
    wakeup_0_cfg.pull_up_enable=1;//使能上拉
    wakeup_0_cfg.pull_down_enable=0;
    luat_pm_wakeup_pad_set(1,LUAT_PM_WAKEUP_PAD_0,&wakeup_0_cfg);
    luat_pm_wakeup_pad_set(1,LUAT_PM_WAKEUP_PAD_3,&wakeup_0_cfg);
    luat_pm_wakeup_pad_set(1,LUAT_PM_WAKEUP_PAD_4,&wakeup_0_cfg);
}

/**********************************WAKEUP 中断测试****************************************************/
void luat_vbus_task(void*param)
{
    Wakeup0_Fun_Init();
    while (1)
    {
        luat_rtos_task_sleep(1000); 
    } 
}

void luat_vbus_task_demo(void)
{
    luat_rtos_task_handle vbus_task_handle;
    luat_rtos_task_create(&vbus_task_handle,4*1024,50,"vbus_task",luat_vbus_task,NULL,NULL);
}

INIT_TASK_EXPORT(luat_vbus_task_demo,"1");