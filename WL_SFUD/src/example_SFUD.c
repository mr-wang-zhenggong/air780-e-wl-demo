#include "common_api.h"
#include "luat_rtos.h" //luat 头文件 封装FreeRTOS
#include "luat_debug.h"//luat DBUG 库
#include "luat_gpio.h"// luat gpio 库
#include "luat_spi.h"


//本例程，主要测试SFUD 测试开发板 Air780E 云喇叭开发板
/**********************************SPI flash 芯片电源控制引脚******************************************/

void Flash_En_Init(void)
{
    luat_gpio_cfg_t Flash_En_cfg;
    luat_gpio_set_default_cfg(&Flash_En_cfg);
    Flash_En_cfg.pin=26;
    Flash_En_cfg.mode=Luat_GPIO_OUTPUT;
    luat_gpio_open(&Flash_En_cfg);
    luat_gpio_
}

void Wakeup_callback_Fun(LUAT_PM_WAKEUP_PAD_E num)
{
    LUAT_DEBUG_PRINT("LUAT_PM_WAKEUP_PAD_E%d",num);
}

/**********************************WAKEUP 中断回调函数************************************************/


void Wakeup0_Fun_Init(void) //Wakeup 0,3,4 初始化
{
    luat_pm_wakeup_pad_set_callback(Wakeup_callback_Fun);
    luat_pm_wakeup_pad_cfg_t wakeup_0_cfg;
    wakeup_0_cfg.neg_edge_enable=1;//使能下降沿
    wakeup_0_cfg.pos_edge_enable=0;
    wakeup_0_cfg.pull_up_enable=1;//使能上拉
    wakeup_0_cfg.pull_down_enable=0;
    luat_pm_wakeup_pad_set(1,LUAT_PM_WAKEUP_PAD_0,&wakeup_0_cfg);
    luat_pm_wakeup_pad_set(1,LUAT_PM_WAKEUP_PAD_3,&wakeup_0_cfg);
    luat_pm_wakeup_pad_set(1,LUAT_PM_WAKEUP_PAD_4,&wakeup_0_cfg);
}

/**********************************WAKEUP 中断测试****************************************************/
void luat_vbus_task(void*param)
{
    Wakeup0_Fun_Init();
    while (1)
    {
        luat_rtos_task_sleep(1000); 
    } 
}

void luat_vbus_task_demo(void)
{
    luat_rtos_task_handle vbus_task_handle;
    luat_rtos_task_create(&vbus_task_handle,4*1024,50,"vbus_task",luat_vbus_task,NULL,NULL);
}

INIT_TASK_EXPORT(luat_vbus_task_demo,"1");