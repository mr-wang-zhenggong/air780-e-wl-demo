#include "common_api.h"
#include "luat_rtos.h" //luat 头文件 封装FreeRTOS
#include "luat_debug.h"//luat DBUG 库
#include "luat_pm.h" //电源管理库
#include "luat_adc.h"

//本例程，主要测试开机原因，VBUS的状态，powerkey 按键

luat_pm_pwrkey_callback_t luat_pwrkey_call_fun(LUAT_PM_POWERKEY_MODE_E status)
{
    if (LUAT_PM_PWRKEY_LONGPRESS==status)
    {
        luat_pm_poweroff();//长按三秒实现关机函数
    }
    
}

void luat_vbus_task(void*param)
{
    int val, val2;
    LUAT_DEBUG_PRINT("poweron_reason  %d",luat_pm_get_poweron_reason());
    uint8_t vbus_status;
    luat_pm_get_vbus_status(&vbus_status);
    LUAT_DEBUG_PRINT("vbus_status  %d",vbus_status);
    while (1)
    {
        luat_rtos_task_sleep(1000);
        luat_adc_read(LUAT_ADC_CH_VBAT, &val, &val2);
        LUAT_DEBUG_PRINT("vbat: adc 原始值 %d, 电压 %d 毫伏",val, val2);
    }
    luat_adc_close(LUAT_ADC_CH_VBAT);
    
}

void luat_vbus_task_demo(void)
{
    luat_adc_open(LUAT_ADC_CH_VBAT,NULL);

    luat_pm_pwrkey_cfg_t pwrkey_cfg={0};
    pwrkey_cfg.long_press_timeout=3000;//长按的时间
    pwrkey_cfg.repeat_timeout=3000;
    luat_pm_set_pwrkey(LUAT_PM_PWRKEY_WAKEUP_LOWACTIVE_MODE,true,&pwrkey_cfg,luat_pwrkey_call_fun);
    luat_rtos_task_handle vbus_task_handle;
    luat_rtos_task_create(&vbus_task_handle,4*1024,50,"vbus_task",luat_vbus_task,NULL,NULL);
}

INIT_TASK_EXPORT(luat_vbus_task_demo,"1");