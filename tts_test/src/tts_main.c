#include "common_api.h"
#include "luat_rtos.h"
#include "luat_audio_play_ec618.h"
#include "luat_i2s_ec618.h"
#include "ivTTSSDKID_all.h"
#include "ivTTS.h"

#include "luat_gpio.h"
#include "luat_debug.h"

//AIR780E+TM8211开发板配置
#define CODEC_PWR_PIN HAL_GPIO_12
#define CODEC_PWR_PIN_ALT_FUN	4
#define PA_PWR_PIN HAL_GPIO_25
#define PA_PWR_PIN_ALT_FUN	0
#define LED2_PIN	HAL_GPIO_24
#define LED2_PIN_ALT_FUN	0
#define LED3_PIN	HAL_GPIO_23
#define LED3_PIN_ALT_FUN	0
#define LED4_PIN	HAL_GPIO_27
#define LED4_PIN_ALT_FUN	0
#define CHARGE_EN_PIN	HAL_GPIO_2
#define CHARGE_EN_PIN_ALT_FUN	0

static HANDLE g_s_delay_timer;
void codec_ctrl(uint8_t onoff)
{
	if (1 == onoff)
	{
	luat_gpio_set(CODEC_PWR_PIN, 1);
	}
	else
	{
	luat_gpio_set(CODEC_PWR_PIN, 0);
	}
}

void app_pa_on(uint32_t arg)
{
	luat_gpio_set(PA_PWR_PIN, 1);
}

void audio_event_cb(uint32_t event, void *param)
{
	LUAT_DEBUG_PRINT("%d", event);
	switch(event)
	{
	case LUAT_MULTIMEDIA_CB_AUDIO_DECODE_START://开始解码
		LUAT_DEBUG_PRINT("LUAT_MULTIMEDIA_CB_AUDIO_DECODE_START");
		codec_ctrl(1);//打开codec 电源
		luat_audio_play_write_blank_raw(0, 3, 1);//开头插入3段空白，为了保证播放前codec 已经打开
		break;
	case LUAT_MULTIMEDIA_CB_AUDIO_OUTPUT_START://开始输出解码后的文件
		luat_rtos_timer_start(g_s_delay_timer, 100, 0, app_pa_on, NULL);//延时100ms 打开PA
		LUAT_DEBUG_PRINT("LUAT_MULTIMEDIA_CB_AUDIO_OUTPUT_START");
		break;
	case LUAT_MULTIMEDIA_CB_TTS_INIT://TTS 完成初始化
		LUAT_DEBUG_PRINT("LUAT_MULTIMEDIA_CB_TTS_INIT");
		break;
	case LUAT_MULTIMEDIA_CB_TTS_DONE://TTS 编码完成
	    LUAT_DEBUG_PRINT("LUAT_MULTIMEDIA_CB_TTS_DONE");
		if (!luat_audio_play_get_last_error(0))
		{
			luat_audio_play_write_blank_raw(0, 1, 0);
		}
		break;
	case LUAT_MULTIMEDIA_CB_AUDIO_DONE://底层驱动播放完成

		luat_rtos_timer_stop(g_s_delay_timer);
		LUAT_DEBUG_PRINT("audio play done, result=%d!", luat_audio_play_get_last_error(0));
		luat_gpio_set(PA_PWR_PIN, 0);
		codec_ctrl(0);
		//如果用软件DAC，打开下面的2句注释，消除POP音和允许进低功耗
//		luat_rtos_task_sleep(10);
//		SoftDAC_Stop();
		break;
	}
}

void audio_data_cb(uint8_t *data, uint32_t len, uint8_t bits, uint8_t channels)
{
	//这里可以对音频数据进行软件音量缩放，或者直接清空来静音
	//软件音量缩放参考HAL_I2sSrcAdjustVolumn
	//LUAT_DEBUG_PRINT("%x,%d,%d,%d", data, len, bits, channels);
}




static void demo_task(void *arg)
{
	size_t total = 0, used = 0, max_used = 0;
	// 中文测试用下面的
	char tts_string[] = "新年快乐";
	// 英文测试用下面的
	// char tts_string[] = "hello world, now test once";
	luat_rtos_timer_create(&g_s_delay_timer);
    luat_audio_play_global_init(audio_event_cb, audio_data_cb, NULL, luat_audio_play_tts_default_fun, NULL);
	ivCStrA sdk_id = AISOUND_SDK_USERID_16K;
	luat_audio_play_tts_set_resource((void*)ivtts_16k, (void*)sdk_id, NULL);
	luat_i2s_base_setup(0, I2S_MODE_MSB, I2S_FRAME_SIZE_16_16);
    while(1)
    {	
		luat_audio_play_tts_text(0, tts_string, sizeof(tts_string));
		luat_rtos_task_sleep(5000);
		luat_meminfo_sys(&total, &used, &max_used);
    	LUAT_DEBUG_PRINT("meminfo total %d, used %d, max_used%d",total, used, max_used);
    }
}

static void test_audio_demo_init(void)
{
	luat_rtos_task_handle task_handle;
	luat_gpio_cfg_t gpio_cfg;
	luat_gpio_set_default_cfg(&gpio_cfg);

	gpio_cfg.pin = LED2_PIN;
	luat_gpio_open(&gpio_cfg);
	gpio_cfg.pin = LED3_PIN;
	luat_gpio_open(&gpio_cfg);
	gpio_cfg.pin = LED4_PIN;
	luat_gpio_open(&gpio_cfg);

	gpio_cfg.pin = CHARGE_EN_PIN;
	luat_gpio_open(&gpio_cfg);

	gpio_cfg.pin = PA_PWR_PIN;
	luat_gpio_open(&gpio_cfg);
	gpio_cfg.pin = CODEC_PWR_PIN;
	gpio_cfg.alt_fun = CODEC_PWR_PIN_ALT_FUN;
	luat_gpio_open(&gpio_cfg);
	luat_rtos_task_create(&task_handle, 2048, 20, "test", demo_task, NULL, 0);
}

INIT_TASK_EXPORT(test_audio_demo_init, "1");
