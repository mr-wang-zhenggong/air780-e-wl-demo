#include "common_api.h"
#include "luat_rtos.h" //luat 头文件 封装FreeRTOS
#include "luat_debug.h"//luat DBUG 库
#include "luat_gpio.h"//luat GPIO 库
#include "platform_define.h"
void NET_LED_Init(void)
{
    luat_gpio_cfg_t Net_led_struct;
    luat_gpio_set_default_cfg(&Net_led_struct);//坑，不设置默认参数，无法点亮
    Net_led_struct.pin=HAL_GPIO_27;
    Net_led_struct.pull=Luat_GPIO_PULLDOWN;
    Net_led_struct.mode=Luat_GPIO_OUTPUT;
    Net_led_struct.output_level=Luat_GPIO_LOW;
    luat_gpio_open(&Net_led_struct);
}

int gpio_level_irq(void *data, void* args)
{
	int pin = (int)data;
	LUAT_DEBUG_PRINT("pin:%d, level:%d,", pin, luat_gpio_get(pin));
    if (luat_gpio_get(HAL_GPIO_27)==0)
    {
        luat_gpio_set(HAL_GPIO_27,1);
    }
    else
    {
        luat_gpio_set(HAL_GPIO_27,0);
    }
}

void KEY_Fun3_Init(void)
{
    luat_gpio_cfg_t key_fun_struct;
    luat_gpio_set_default_cfg(&key_fun_struct);
    key_fun_struct.pin=HAL_GPIO_20;
    key_fun_struct.pull=Luat_GPIO_PULLUP;
    key_fun_struct.mode=Luat_GPIO_IRQ;
    key_fun_struct.irq_type=LUAT_GPIO_FALLING_IRQ;
    key_fun_struct.irq_cb=gpio_level_irq;
    luat_gpio_open(&key_fun_struct);
}

static void NET_LED_Task(void *param)
{
    NET_LED_Init();
    KEY_Fun3_Init();
    while (1)
    {
        luat_rtos_task_sleep(500);
    }
    
}

void NET_LED_Demo(void)
{
    luat_rtos_task_handle Net_led_task_handler;
    luat_rtos_task_create(&Net_led_task_handler,2*1024,50,"net_led_task",NET_LED_Task,NULL,NULL);
}


INIT_TASK_EXPORT(NET_LED_Demo,"1");