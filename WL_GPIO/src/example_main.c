#include "common_api.h"
#include "luat_rtos.h" //luat 头文件 封装FreeRTOS
#include "luat_debug.h"//luat DBUG 库
#include "luat_gpio.h"//luat GPIO 库
#include "platform_define.h"
#include "pad.h"

/********************************************GPIO 点亮led GPIO27 start*******************************************/
void NET_LED_Init(void)
{
    luat_gpio_cfg_t Net_led_struct;
    luat_gpio_set_default_cfg(&Net_led_struct);//坑，不设置默认参数，无法点亮
    Net_led_struct.pin=HAL_GPIO_27;
    Net_led_struct.pull=Luat_GPIO_PULLDOWN;
    Net_led_struct.mode=Luat_GPIO_OUTPUT;
    Net_led_struct.output_level=Luat_GPIO_LOW;
    luat_gpio_open(&Net_led_struct);
}

static void NET_LED_Task(void *param)
{
    NET_LED_Init();
    while (1)
    {
        luat_gpio_set(HAL_GPIO_27,0);
        luat_rtos_task_sleep(500);
        luat_gpio_set(HAL_GPIO_27,1);
        luat_rtos_task_sleep(500);
    }
    
}

void NET_LED_Demo(void)
{
    luat_rtos_task_handle Net_led_task_handler;
    luat_rtos_task_create(&Net_led_task_handler,2*1024,50,"net_led_task",NET_LED_Task,NULL,NULL);
}
/********************************************GPIO 点亮led GPIO27 end *******************************************/

/********************************************GPIO 复用 GPIO15 GPIO14 start*******************************************/

//GPIO 15 14 原本是I2C 接口（780E） 这里复用为GPIO
//GPIO 15 14 复用对应的是ALT_Func4 具体复用功能参考Air780E&Air600E_GPIO_table_1110.pdf

void GPIO15_14_Func4_Init(void)
{
    luat_gpio_cfg_t GPIO15_14_struct;
    luat_gpio_set_default_cfg(&GPIO15_14_struct);//坑，不设置默认参数，无法点亮
    GPIO15_14_struct.pin=HAL_GPIO_14|HAL_GPIO_15;
    GPIO15_14_struct.pull=Luat_GPIO_PULLDOWN;
    GPIO15_14_struct.mode=Luat_GPIO_OUTPUT;
    GPIO15_14_struct.alt_fun=PAD_MUX_ALT4;
    GPIO15_14_struct.output_level=Luat_GPIO_LOW;
    luat_gpio_open(&GPIO15_14_struct);
}

static void GPIO15_14_Func4_Task(void *param)
{
    GPIO15_14_Func4_Init();
    while (1)
    {
        luat_gpio_set(HAL_GPIO_14|HAL_GPIO_15,0);
        luat_rtos_task_sleep(500);
        luat_gpio_set(HAL_GPIO_14|HAL_GPIO_15,1);
        luat_rtos_task_sleep(500);
    }
    
}

void GPIO15_14_Demo(void)
{
    luat_rtos_task_handle GPIO15_14_task_handler;
    luat_rtos_task_create(&GPIO15_14_task_handler,2*1024,50,"GPIO15_14_task",GPIO15_14_Func4_Task,NULL,NULL);
}

/********************************************GPIO 复用 GPIO15 GPIO14 end *******************************************/

INIT_TASK_EXPORT(NET_LED_Demo,"1");
INIT_TASK_EXPORT(GPIO15_14_Demo,"2");