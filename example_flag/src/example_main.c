/*
 * Copyright (c) 2022 OpenLuat & AirM2M
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#include "common_api.h"
#include "luat_rtos.h"
#include "luat_mem.h"
#include "luat_debug.h"

luat_rtos_task_handle task1_handle;
luat_rtos_task_handle task2_handle;
luat_rtos_task_handle task3_handle;
luat_rtos_flag_t example_flag_t;
#define BIT_0	( 1 << 0 )
#define BIT_4	( 1 << 4 )


static void task1(void *param)
{
	uint32_t flags;
	int ret=-1;
	while(1)
	{
		luat_rtos_task_sleep(500);
		ret = luat_rtos_flag_wait(example_flag_t, BIT_0 | BIT_4, LUAT_FLAG_OR_CLEAR, &flags, LUAT_WAIT_FOREVER);
		if (0 == ret)
		{
			if ((flags & (BIT_0 | BIT_4)) == (BIT_0 | BIT_4))
			{
				LUAT_DEBUG_PRINT("returned because both bits were set.");
			}
			else if ((flags & BIT_0) != 0)
			{
				LUAT_DEBUG_PRINT("returned because both bit0  were set.");
			}
			else if ((flags & BIT_4) != 0)
			{
				LUAT_DEBUG_PRINT("returned because both bit1 were set.");
			}
			else
			{
				LUAT_DEBUG_PRINT("returned failed");
			}
		}

		LUAT_DEBUG_PRINT("task1 loop");
	}
}

static void task2(void *param)
{
	while(1)
	{
		luat_rtos_task_sleep(1000);
		luat_rtos_flag_release(example_flag_t, BIT_0 | BIT_4,LUAT_FLAG_OR_CLEAR);
		LUAT_DEBUG_PRINT("task2 loop");
		
	}
}

static void task_demoE_init(void)
{
	
	luat_rtos_flag_create(&example_flag_t);
	luat_rtos_task_create(&task1_handle, 2*1024, 50, "task1", task1, NULL, 0);
}

static void task_demoF_init(void)
{
	luat_rtos_task_create(&task2_handle, 2*1024, 50, "task2", task2, NULL, 0);
}


INIT_TASK_EXPORT(task_demoE_init, "1");

INIT_TASK_EXPORT(task_demoF_init, "2");

